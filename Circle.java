package shapes2d;

public class Circle {
    double radius;

    public Circle(double r) {
        radius = r;
    }

    public double getR() {
        return radius;
    }

    public double Circum() {
        return 2 * 3.14159 * radius;
    }

    public double Area() {
        return radius * radius * 3.14159;
    }

    @Override
    public String toString() {
        return radius + "";
    }
}
