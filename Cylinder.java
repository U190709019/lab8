package shapes3d;
import shapes2d.*;

public class Cylinder extends Circle {
    double height;

    public Cylinder(double r, double h) {
        super(r);
        height = h;
    }

    public double Area() {
        return 2*super.Area()+Circum()*height;
    }

    public double Volume() {
        return super.Area()*height;
    }

    @Override
    public String toString() {
        return height + "";
    }
}
