import shapes2d.Circle;
import shapes2d.Square;
import shapes3d.Cylinder;
import shapes3d.Cube;

public class Test {
    public static void main(String[] args) {
        Circle c1 = new Circle(4.00);
        Square s1 = new Square(3);
        Cylinder ci1 = new Cylinder(3.00, 5.50);
        Cube cu1 = new Cube(4);

        System.out.println("Circle thing: "+c1.Area()+", "+c1.Circum());
        System.out.println("Square thing: "+s1.Area());
        System.out.println("Cylinder thing: "+ci1.Area()+", "+ci1.Volume());
        System.out.println("Cube thing: "+cu1.Area()+", "+cu1.Volume());
    }
}
