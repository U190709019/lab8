package shapes2d;

public class Square {
    public int e;

    public Square(int edge) {
        e = edge;
    }

    public int Area() {
        return e * e;
    }

    @Override
    public String toString() {
        return e + "";
    }
}
