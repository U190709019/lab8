package shapes3d;
import shapes2d.*;

public class Cube extends Square {

    public Cube(int edge) {
        super(edge);
    }

    public int Area() {
        return super.Area() * 6;
    }

    public int Volume() {
        return super.Area() * super.e;
    }
    
    @Override
    public String toString() {
        return super.e + "";
    }
}
